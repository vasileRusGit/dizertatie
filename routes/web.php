<?php

/*-----------------------------------------------------------------------------------------------------------------10101
 *
 * Routes that not require auth
 *
 ---------------------------------------------------------------------------------------------------------------------*/
// auth
Route::patch('/register', [\App\Http\Controllers\Admin\Auth\Register::class, 'resolve']);
Route::patch('/login', [\App\Http\Controllers\Admin\Auth\Login::class, 'resolve']);

// check auth
Route::get('/check-auth', [\App\Http\Controllers\Admin\Auth\Check::class, 'resolve']);

/*-----------------------------------------------------------------------------------------------------------------10101
 *
 * Routes that require auth
 *
 ---------------------------------------------------------------------------------------------------------------------*/
Route::middleware('auth.check')->group(function () {
    // user profile
    Route::prefix('user')->group(function () {
        Route::get('/get', [\App\Http\Controllers\Admin\User\Get::class, 'resolve']);
        Route::patch('/update', [\App\Http\Controllers\Admin\User\Update::class, 'resolve']);
    });

    // design filter
    Route::post('/filter/design', [\App\Http\Controllers\Filter\Design::class, 'resolve']);
    Route::post('/filter/save', [\App\Http\Controllers\Filter\Save::class, 'resolve']);
    Route::get('/filters/get', [\App\Http\Controllers\Filter\Get::class, 'resolve']);
    Route::delete('/filter/delete', [\App\Http\Controllers\Filter\Delete::class, 'resolve']);
    Route::get('/butterworth/transfer/function/values', [\App\Http\Controllers\TransferFunctionValues\Butterworth::class, 'resolve']);
});

/*-----------------------------------------------------------------------------------------------------------------10101
 *
 * Default route for instantiating the vue app
 *
 ---------------------------------------------------------------------------------------------------------------------*/
Route::get('{path}', function () {
    return view('index');
})->where(['path' => '.*']);