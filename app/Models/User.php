<?php

namespace App\Models;

use App\Models\Auth\Token;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * @var string
     */
    protected $table = "users";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token' , 'created_at', 'updated_at', 'id',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function accessToken()
    {
        return $this->hasOne(Token::class, 'user_id');
    }

    /**
     * @param string $uid
     *
     * @return User
     *
     * @throws ModelNotFoundException
     */
    public static function findByUid(string $uid) : User
    {
        return self::where('uid', $uid)->firstOrFail();
    }

    /**
     * @param string $uid
     *
     * @return User
     *
     * @throws ModelNotFoundException
     */
    public static function findByEmail(string $email) : User
    {
        return self::where('email', $email)->firstOrFail();
    }
}
