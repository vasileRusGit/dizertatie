<?php

namespace App\Models\Filter;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Chart
 *
 * @package App\Models\Filter
 */
class Chart extends Model
{
    /**
     * @var string
     */
    protected $table = 'filter_charts';

    /**
     * @var array
     */
    protected $hidden = [
        'id', 'user_id','updated_at'
    ];

    /**
     * @param User $user
     *
     * @return mixed
     */
    public static function findByUser(User $user)
    {
        return self::where('user_id', $user->id)->get();
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public static function findByName($name)
    {
        return self::where('name', $name)->firstOrFail();
    }

    /**
     * @param string $uid
     *
     * @return mixed
     */
    public static function findByUid(string $uid)
    {
        return self::where('uid', $uid)->firstOrFail();
    }
}
