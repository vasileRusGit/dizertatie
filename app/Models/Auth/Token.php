<?php

namespace App\Models\Auth;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Token
 *
 * @package App\Models\Auth
 */
class Token extends Model
{
    /**
     * @var string
     */
    protected $table = "user_auth_token";
}
