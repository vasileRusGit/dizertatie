<?php

namespace App\BusinessModel\Admin\Auth;

use App\BusinessModel\Helpers\Uid;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class Token
 *
 * @package App\BusinessModel\Admin\Auth
 */
class Token
{
    /**
     * @var string
     */
    protected $plainToken;

    /**
     * Token constructor.
     *
     * @param User $user
     */
    public function __construct (User $user)
    {
        $this->user = $user;
        $this->plainToken = Uid::generate();
    }

    /**
     * @return bool
     */
    public function resolve () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Check if user has access token
         *
         -------------------------------------------------------------------------------------------------------------*/
        $accessToken = $this->user->accessToken;
        if($accessToken && isset($accessToken->token))
        {
            $accessToken->delete();
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Generate new access token
         *
         -------------------------------------------------------------------------------------------------------------*/
        if($this->newToken())
        {
            return true;
        }

        return false;
    }

    /**
     * @return \App\Models\Auth\Token
     */
    public function newToken() : \App\Models\Auth\Token
    {
        $accessToken = new \App\Models\Auth\Token();
        $accessToken->user_id = $this->user->id;
        $accessToken->token = Hash::make($this->plainToken);
        $accessToken->save();

        return $accessToken;
    }

    /**
     * @return string
     */
    public function getPlainToken()
    {
        return $this->plainToken;
    }
}