<?php

namespace App\BusinessModel\Admin\Filter\Calculate;

/**
 * Class Order
 * @package App\BusinessModel\Admin\Filter\Calculate
 */
class Order
{
    /**
     * @var array
     */
    protected $params;

    /**
     * Order constructor.
     * @param array $params
     */
    public function __construct (array $params)
    {
        $this->params = $params;
    }

    /**
     * @return int
     */
    public function calculateOrder () : int
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate atenuarea in banda te oprire & atenuarea in banda de trecere
         *
         -------------------------------------------------------------------------------------------------------------*/
        $atenuareaInBandaDeOprire = pow(10, $this->params['atenuarea_in_banda_de_oprire'] / 10) - 1;
        $atenuareaInBandaDeTrecere = pow(10, $this->params['atenuarea_in_banda_de_trecere'] / 10) - 1;

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate order coeficiants
         *
         -------------------------------------------------------------------------------------------------------------*/
        $Ka = sqrt($atenuareaInBandaDeOprire / $atenuareaInBandaDeTrecere);
        $Kf = $this->params['frecventa_de_oprire'] / $this->params['frecventa_de_trecere'];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate order, must be greater than the n. eg n = 7.3, order = 8
         *
         -------------------------------------------------------------------------------------------------------------*/
        $order = intval(log($Ka) / log($Kf));

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Else return order.
         *
         -------------------------------------------------------------------------------------------------------------*/

        return $order + 1;
    }
}