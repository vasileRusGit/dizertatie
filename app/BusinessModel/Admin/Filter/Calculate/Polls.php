<?php

namespace App\BusinessModel\Admin\Filter\Calculate;

use phpDocumentor\Reflection\Types\Integer;

/**
 * Class Polls
 * @package App\BusinessModel\Admin\Filter\Calculate
 */
class Polls
{
    /**
     * @var array
     */
    protected $polls;

    /**
     * @var int
     */
    protected $order;

    /**
     * Polls constructor.
     * @param int $order
     */
    public function __construct (int $order)
    {
        $this->order = $order;
    }

    /**
     * @return array
     */
    public function calculatePols () : array
    {
        $polls = [];
        $values = [];
        for ( $i = 0; $i <= $this->order - 1; $i++ )
        {
            $real = cos((($this->order + 2 * $i + 1) * pi()) / (2 * $this->order));
            $imaginary = sin((($this->order + 2 * $i + 1) * pi()) / (2 * $this->order));
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if imaginary value is negative
             *
             ---------------------------------------------------------------------------------------------------------*/
            $values[$i]['s'] = 's';
            $values[$i]['real'] = $real;
            if ($imaginary < 0)
            {
                $polls[$i] = $real . '-' . abs($imaginary) . 'i';
                $values[$i]['img'] = '-' . abs($imaginary) . 'i';
            }
            else
            {
                $polls[$i] = $real . '+' . $imaginary . 'i';
                $values[$i]['img'] = '+' . $imaginary . 'i';
            }
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Transform polls into individual arrays
             *
             ---------------------------------------------------------------------------------------------------------*/
            $var = '-' . $values[$i]['real'];
            if (strpos($var, '--') !== false || strpos($var, '-+') !== false || strpos($var, '+-') !== false)
            {
                if (strpos($var, '--') !== false)
                {
                    $values[$i]['real'] = str_replace('--', '+', $var);
                }
                if (strpos($var, '-+') !== false)
                {
                    $values[$i]['real'] = str_replace('-+', '-', $var);
                }
                if (strpos($var, '+-') !== false)
                {
                    $values[$i]['real'] = str_replace('+-', '-', $var);
                }
            }
            else
            {
                $values[$i]['real'] = $var;
            }
        }

        $this->polls = array_reverse($values);

        return $this->polls;
    }

    /**
     * @return array
     */
    public function calculateTransferFunctionCoefficients () : array
    {
        $polls = count($this->polls) - 1;
        for ( $i = 0; $i <= $polls; $i++ )
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if order is odd or even
             *
             ---------------------------------------------------------------------------------------------------------*/
            if (count($this->polls) % 2 !== 0)
            {
                $stripedArrayKye = (int) floor(count($this->polls) / 2);
                unset($this->polls[$stripedArrayKye]);
            }
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Find individual polls
             *
             ---------------------------------------------------------------------------------------------------------*/
            $polls = $this->polls;
            for ( $i = 0; $i < count($this->polls) / 2; $i++ )
            {
                if (!empty($polls))
                {
                    $elements = $this->getFirstAndLastElementsFromArray($polls);
                    $data[$i] = $this->multiplyRealAndImaginaryData($elements['firstPolElements'], $elements['lastPolElements']);
                    $polls = $elements['remaining_polls'];
                }
            }

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * If order is odd add (s+1)
             *
             ---------------------------------------------------------------------------------------------------------*/
            if($this->order % 2 !== 0)
            {
                $data[] = [
                    's' => 's',
                    'real' => 1
                ];
            }

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * multiply real values that contains 's'
             *
             ---------------------------------------------------------------------------------------------------------*/
            $coefficients = $this->multiplyRealData($data);

            return $coefficients;
        }
    }

    /**
     * @param array $firstElemValues
     * @param array $secondElemValues
     *
     * @return array
     */
    public function multiplyRealData (array $data) : array
    {
        if(count($data) > 1)
        {
            $orderCalculation = count($data) - 1;
        }
        else
        {
            $orderCalculation = count($data);
        }
        $elements = $data;

        for($i = 0; $i <= $orderCalculation - 1; $i++)
        {
            if($this->order <= 4)
            {
                $values = $this->getParamDataFromSingle($elements);
            }
            if($this->order > 4)
            {
                $values = $this->getParamDataFromArray($elements);
            }

            unset($elements[0]);
            unset($elements[1]);
            $elements = array_values($elements);
            $elements[] = $values;
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate the coefficients for 's' values
         *
         -------------------------------------------------------------------------------------------------------------*/
        if(is_array(reset($elements)['s_and_something']))
        {
            foreach ( reset($elements)['s_and_something'] as $value )
            {
                if(is_array($value))
                {
                    foreach($value as $item)
                    {
                        $numberOfS = substr_count($item, "s");
                        $newValue = str_replace('s', '', $item);

                        /*---------------------------------------------------------------------------------------------10101
                         *
                         * Explode '*' and multiply values
                         *
                         -------------------------------------------------------------------------------------------------*/
                        $numbers = explode('*', str_replace('**', '*', $newValue));
                        $tempValue = 1;
                        foreach($numbers as $number)
                        {
                            if($number === '')
                            {
                                $number = 1;
                            }
                            $tempValue *= $number;
                        }
                        $coefficients[$numberOfS][] = $tempValue;
                    }
                }
                else
                {
                    $numberOfS = substr_count($value, "s");
                    $newValue = str_replace('s', '', $value);

                    /*-------------------------------------------------------------------------------------------------10101
                     *
                     * Explode '*' and multiply values
                     *
                     -----------------------------------------------------------------------------------------------------*/
                    $numbers = explode('*', str_replace('**', '*', $newValue));
                    $tempValue = 1;
                    foreach($numbers as $number)
                    {
                        if($number === '')
                        {
                            $number = 1;
                        }
                        $tempValue *= $number;
                    }
                    $coefficients[$numberOfS][] = $tempValue;
                }
            }
        }
        else
        {
            $numberOfS = substr_count(reset($elements)['s_and_something'], "s");
            $newValue = str_replace('s', '', reset($elements)['s_and_something']);

            /*-------------------------------------------------------------------------------------------------10101
             *
             * Explode '*' and multiply values
             *
             -----------------------------------------------------------------------------------------------------*/
            $numbers = explode('*', str_replace('**', '*', $newValue));
            $tempValue = 1;
            foreach($numbers as $number)
            {
                if($number === '')
                {
                    $number = 1;
                }
                $tempValue *= $number;
            }
            $coefficients[$numberOfS][] = $tempValue;
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Add coefficients between them self
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach ( $coefficients as $key => $items )
        {
            $result = 0;
            foreach ( $items as $value )
            {
                $result += $value;
            }
            $coefficientsOfS[$key] = $result;
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Get from initial array the 's0' and last pow of 's'
         *
         -------------------------------------------------------------------------------------------------------------*/
        $finalCoefficients = $coefficientsOfS;
        unset($values['s_and_something']);

        foreach ( $values as $item )
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * If string has 's' is the last 's' pow and the value wold be the value after stripping 's',
             * If no number then is 1.
             * Else is the real number
             *
             ---------------------------------------------------------------------------------------------------------*/
            if (strpos($item, 's') !== false)
            {
                $tempValueForLastS = str_replace('s', '', $item);
                if (strlen($tempValueForLastS) == 0)
                {
                    $valueToBeSaved = 1;
                }
                else
                {
                    $valueToBeSaved = 'error';
                }
            }
            else
            {
                $valueToBeSaved = $item;
            }
            $finalCoefficients[substr_count($item, "s")] = $valueToBeSaved;
        }
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Sort coefficients after key value
         *
         -------------------------------------------------------------------------------------------------------------*/
        ksort($finalCoefficients);

        return $finalCoefficients;
    }

    /**
     * @param string $first
     * @param string $second
     * @return array
     */
    public function getParamDataFromSingle(array $elements) : array
    {
        $values = [];
        if(isset($elements[1]))
        {
            foreach ( $elements[0] as $first )
            {
                foreach ( $elements[1] as $second )
                {
                    $values = $this->individualCalc($first, $second, $values);
                }
            }
        }
        else
        {
            $values = $elements[0];
        }

        return $values;
    }

    /**
     * @param $first
     * @param $second
     * @return array
     */
    public function getParamDataFromArray(array $elements) : array
    {
        $values = [];
        foreach ( $elements[0] as $first )
        {
            foreach ( $elements[1] as $second )
            {
                if(!is_array($first) && is_array($second))
                {
                    foreach ( $second as $param )
                    {
                        if(is_array($param)){
                            $param = reset($param);
                        }

                        $values = $this->individualCalc($first, $param, $values);
                    }
                }
                else if(is_array($first) && !is_array($second))
                {
                    foreach($first as $param)
                    {
                        if(is_array($param)){
                            $param = reset($param);
                        }

                        $values = $this->individualCalc($second, $param, $values);
                    }
                }
                else if(is_array($first) && is_array($second))
                {
                    foreach($first as $paramFirst)
                    {
                        foreach($second as $paramSecond)
                        {
                            if(is_array($paramFirst)){
                                $paramFirst = reset($paramFirst);
                            }
                            if(is_array($paramSecond)){
                                $paramSecond = reset($paramSecond);
                            }

                            $values = $this->individualCalc($paramFirst, $paramSecond, $values);
                        }
                    }
                }
                else if(!is_array($first) && !is_array($second))
                {
                    $values = $this->individualCalc($first, $second, $values);
                }
            }
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * check if real is greater then 0.98, round to 1
         *
         -------------------------------------------------------------------------------------------------------------*/
        if($values['real'] > 0.98)
        {
            $values['real'] = round($values['real']);
        }

        return $values;
    }

    /**
     * @param string $first
     * @param string $second
     * @return string
     */
    public function individualCalc(string $first, string $second, array $values) : array
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * If one of the elements has 's'
         * Else real number
         *
         -------------------------------------------------------------------------------------------------------------*/
        if (strpos($first, 's') !== false || strpos($second, 's') !== false)
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * If elements has just 's', by checking if elem has number
             * Else have 's' and value
             *
             ---------------------------------------------------------------------------------------------------------*/
            if (!preg_match('/[0-9]/', $first) && !preg_match('/[0-9]/', $second))
            {
                $values['s'] = $first . $second;
            }
            else
            {
                /*---------------------------------------------------------------------------------10101
                 *
                 * If both elem has 's' and value, then multiply the values and concatenate
                 * Else concatenate.
                 *
                 -------------------------------------------------------------------------------------*/
                if (strpos($first, 's') !== false && strpos($second, 's')
                    !== false && preg_match('/[0-9]/', $first) && preg_match('/[0-9]/', $second))
                {
                    $numberOfS = substr_count($first . $second, "s");
                    $realFirst = str_replace('s', '', $first);
                    $realSecond = str_replace('s', '', $second);
                    /*---------------------------------------------------------------------------------------------10101
                     *
                     * Explode '*' and multiply values
                     *
                     -------------------------------------------------------------------------------------------------*/
                    if(strpos($realFirst, '*') !== false)
                    {
                        $numbers = explode('*', $realFirst);
                        $realFirst = 1;
                        foreach($numbers as $number)
                        {
                            if($number === '')
                            {
                                $number = 1;
                            }
                            $realFirst *= $number;
                        }
                    }
                    if(strpos($realSecond, '*') !== false)
                    {
                        $numbers = explode('*', $realSecond);
                        $realSecond = 1;
                        foreach($numbers as $number)
                        {
                            if($number === '')
                            {
                                $number = 1;
                            }
                            $realSecond *= $number;
                        }
                    }

                    $values['s_and_something'][] = str_repeat("s", $numberOfS) . '*' . $realFirst * $realSecond;
                }
                else
                {
                    $values['s_and_something'][] = $first . '*' . $second;
                }
            }
        }
        else
        {
            $values['real'] = $first * $second;
        }

        return $values;
    }

    /**
     * @param array $firstPollElements
     * @param array $lastPollElements
     *
     * @return array
     */
    public function multiplyRealAndImaginaryData (array $firstPollElements, array $lastPollElements) : array
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Multiply elements between them self
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach ( $firstPollElements as $first )
        {
            foreach ( $lastPollElements as $second )
            {
                // check if one of the elements has 's'
                if (strpos($first, 's') !== false || strpos($second, 's') !== false)
                {
                    // check if both elem has 's'
                    if (strpos($first, 's') !== false && strpos($second, 's') !== false)
                    {
                        $values['s'] = $first . '' . $second;
                    }
                    else
                    {
                        $values['s_and_something'][] = $first . '' . $second;
                    }
                }
                // check elements without 's'
                if (strpos($first, 's') === false && strpos($second, 's') === false)
                {
                    // check if both are real
                    if (strpos($first, 'i') === false && strpos($second, 'i') === false)
                    {
                        $values['real'][] = $first * $second;
                    }
                    // check if both are img
                    if (strpos($first, 'i') !== false && strpos($second, 'i') !== false)
                    {
                        $realFirst = str_replace('i', '', $first);
                        $realSecond = str_replace('i', '', $second);
                        $values['real'][] = -1 * $realFirst * $realSecond;
                    }
                    // check if one of the elements is real and the second one is img
                    if ((strpos($first, 'i') !== false && strpos($second, 'i') === false)
                        || (strpos($second, 'i') !== false && strpos($first, 'i') === false))
                    {
                        $realFirst = str_replace('i', '', $first);
                        $realSecond = str_replace('i', '', $second);
                        $values['img'][] = $realFirst * $realSecond . 'i';
                    }
                }
            }
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Add / Substract multiply elements
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach ( $values as $key => $elements )
        {
            // calculate values that contains 's' and some value
            if ($key === 's_and_something')
            {
                foreach ( $elements as $element )
                {
                    // separate img values from the real one
                    if (strpos($element, 'i') !== false)
                    {
                        $imgElem[] = str_replace('s', '', $element);
                    }
                    else
                    {
                        $realElem[] = str_replace('s', '', $element);
                    }
                }
                // add real values that contains 's'
                $resultReal = 0;
                foreach ( $realElem as $real )
                {
                    $resultReal += $real;
                }
                // add img values that contains 's'
                $resultImg = 0;
                foreach ( $imgElem as $img )
                {
                    $resultImg += str_replace('i', '', $img);
                }
                // check if img and real got 0 as result
                if ($resultImg == 0 || $resultReal == 0)
                {
                    // if real is 0, not save, else save it
                    if ($resultReal != 0)
                    {
                        $values['s_and_something'] = 's' . $resultReal;
                    }
                    // if img is 0, not save, else save it
                    if ($resultImg != 0)
                    {
                        $values['s_and_something'] = 's' . $resultImg;
                    }
                }
            }
            // calculate real values
            if ($key === 'real')
            {
                $result = 0;
                foreach ( $elements as $element )
                {
                    $result += $element;
                }
                $values['real'] = $result;
            }
            // calculate img values
            if ($key === 'img')
            {
                $result = 0;
                foreach ( $elements as $element )
                {
                    $result += str_replace('i', '', $element);
                }
                $values['img'] = $result;
            }
        }
        // add just if imaginary is different from 0, that mean error
        if ($values['img'] !== '0')
        {
            unset($values['img']);
        }

        return $values;
    }

    /**
     * @return array
     */
    public function getFirstAndLastElementsFromArray (array $polls)
    {
        $firstPolElements = reset($polls);
        $lastPolElements = end($polls);
        $remainingPolls = array_slice($polls, 1, -1);

        return [
            'firstPolElements' => $firstPolElements,
            'lastPolElements'  => $lastPolElements,
            'remaining_polls'  => $remainingPolls
        ];
    }
}