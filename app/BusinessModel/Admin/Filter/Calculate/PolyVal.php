<?php

namespace App\BusinessModel\Admin\Filter\Calculate;

use Complex\Complex;

/**
 * Class PolyVal
 * @package App\BusinessModel\Admin\Filter\Calculate
 */
class PolyVal
{

    /**
     * @var mixed
     */
    protected $denumitor;

    /**
     * @var mixed
     */
    protected $numitor;

    /**
     * PolyVal constructor.
     * @param $denormation
     * @param array $numitor
     */
    public function __construct ($denormation, array $numitor)
    {
        $this->denumitor = $denormation;
        $this->numitor = $numitor;
    }

    /**
     *
     */
    public function  calculatePolyValValues ()
    {
        $polyValValues = [];
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate polyval values for the numitor. s->jw
         *
         -------------------------------------------------------------------------------------------------------------*/
        for ( $i = 1; $i <= 100; $i++ )
        {
            $value = '';
            foreach ( $this->numitor as $key => $numitor )
            {
                if(is_array($numitor))
                {
                    $value .= pow($i * $numitor['value'] * pow(10, $numitor['pow']),
                            $numitor['pow_off_s']) . '*' . str_repeat("i", $numitor['pow_off_s']) . '+';
                }
                else
                {
                    $value .= pow($i * $numitor, $key) . '*' . str_repeat("i", $key) . '+';
                }

            }

            // explode result
            $results = explode('+', $value);
            $finalValues = [];
            foreach ( $results as $result )
            {
                if (!empty($result))
                {
                    $matches = explode('*', $result);
                    $real = $matches[0];
                    $imaginary = $matches[1];
                    // calculate imaginary value
                    if (empty($imaginary))
                    {
                        $imaginary = 1;
                    }
                    else
                    {
                        $imaginary = $this->calculateImaginaryValue($imaginary);
                    }
                    // check if imaginary value contains 'i' or not
                    if (strpos($imaginary, 'i') !== false)
                    {
                        $finalValues[] = $real . '*' . $imaginary;
                    }
                    else
                    {
                        $finalValues[] = $real * $imaginary;
                    }
                }
            }

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Separe imaginary and real and add them
             *
             ---------------------------------------------------------------------------------------------------------*/
            $imaginary = 0;
            $real = 0;
            foreach ( $finalValues as $value )
            {
                /*-------------------------------------------------------------------------------------------------10101
                 *
                 * If has i, add imaginary values
                 * Else real values
                 *
                 -----------------------------------------------------------------------------------------------------*/
                if (strpos($value, 'i') !== false)
                {
                    $newValue = str_replace('i', '', str_replace('*', '', $value));
                    if (strpos($newValue, '-') !== false)
                    {
                        $newValue = -1 * str_replace('-', '', $newValue);
                    }
                    $imaginary += $newValue;
                }
                else
                {
                    $newValue = $value;
                    if (strpos($newValue, '-') !== false)
                    {
                        $newValue = -1 * str_replace('-', '', $newValue);
                    }
                    $real += $newValue;
                }
            }

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Add i to the imaginary value
             * If has no sign, that means is plus and add the sign + to the real
             *
             ---------------------------------------------------------------------------------------------------------*/
            if(strpos($real, '-') === false)
            {
                $real = '+' . $real;
            }
            $responseValueTemp = $real . '+' . $imaginary . '*i';
            $polyValValues['frequency'][] = $i;
            $responseValue = str_replace('-+', '-', str_replace('+-', '-', $responseValueTemp));
            $polyValValues['db'][] = $this->multiplyPolyValWithHisConjugate($responseValue);
        }

        return $polyValValues;
    }

    /**
     * @param string $value
     * @return string
     */
    public function multiplyPolyValWithHisConjugate(string $value) : string
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * If has just +
         * Else if has just -
         * Else has a + and a -
         *
         -------------------------------------------------------------------------------------------------------------*/
        if(strpos($value, '+') !== false && strpos($value, '-') === false)
        {
            $result = $this->formatValuesForComplexConjugate($value, '+');
        }
        else if(strpos($value, '+') === false)
        {
            $result = $this->formatValuesForComplexConjugate($value, '-');
        }
        else if(strpos($value, '+') !== false && strpos($value, '-') !== false)
        {
            $result = $this->formatValuesForComplexConjugate($value, '+-');
        }

        return $result;
    }

    /**
     * @param string $values
     * @return string
     */
    public function formatValuesForComplexConjugate(string $values, string $sign) : string
    {
        if($sign === '+-')
        {
            $firstExplode = explode('+', $values);
            if(empty($firstExplode[0]))
            {
                $matches = explode('-', $firstExplode[1]);
                $matches[1] = '-' . $matches[1];
            }
            else
            {
                $matches = $firstExplode;
            }
        }
        else
        {
            $matches = explode($sign, $values);
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate variables
         *
         -------------------------------------------------------------------------------------------------------------*/
        $variables = [];
        foreach($matches as $match)
        {
            if(!empty($match))
            {
                if(strpos($match, 'i') !== false)
                {
                    $variables['img'] = str_replace('i', '', str_replace('*', '',$match));
                }
                else
                {
                    $variables['real'] = str_replace('*', '',$match);
                }
            }
        }

        return $this->calculateComplexConjugate($variables);
    }

    /**
     * @param array $values
     * @return string
     */
    public function calculateComplexConjugate(array $values) : string
    {
        $initialValues = $values;
        $conjugatedValues = [];
        foreach($values as $key => $value)
        {
            $conjugatedValues[$key] = -1 * $value;
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Multiply initial values array with conjugate values
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach($initialValues as $initialKey => $initial)
        {
            foreach($conjugatedValues as $conjugateKey => $conjugate)
            {
                /*-------------------------------------------------------------------------------------------------10101
                 *
                 * Check if at least one element is imaginary
                 *
                 -----------------------------------------------------------------------------------------------------*/
                if($initialKey === 'img' || $conjugateKey === 'img')
                {
                    if($initialKey === 'img' && $conjugateKey === 'img')
                    {
                        $sign = $this->calculateImaginaryValue('ii');
                        $value = $initial * $conjugate *  $sign;
                    }
                    else
                    {
                        $sign = $this->calculateImaginaryValue('i');
                        $value = $initial * $conjugate . $sign;
                    }
                }
                else
                {
                    $value = $initial * $conjugate;
                }

                $multiplyValues[] = $value;
            }
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Add the imaginary values and the real ones
         *
         -------------------------------------------------------------------------------------------------------------*/
        $imgValue = 0;
        $realValue = 0;
        foreach($multiplyValues as $val)
        {
            if(strpos($val, 'i') !== false)
            {
                $imgValue += str_replace('i', '', $val);
            }
            else
            {
                $realValue += $val;
            }
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * If imaginary value is positive add + sign
         *
         -------------------------------------------------------------------------------------------------------------*/
        if(strpos($imgValue, '-') === false)
        {
            $imgValue = '+' . $imgValue;
        }

        $finalValue = $realValue . $imgValue.'i';

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Divide denumitor to complex number
         *
         -------------------------------------------------------------------------------------------------------------*/
        $complex = new Complex($finalValue);
        $result = $complex->divideinto($this->denumitor);

        if($result->getImaginary() > 0)
        {
            return $result->getReal() . '+' .$result->getImaginary() . $result->getSuffix();
//            return sprintf('%.20f', floatval(abs(sprintf('%.20f', floatval($result->getReal())) .
//                sprintf('%.20f', floatval($result->getImaginary())). $result->getSuffix())));
        }
        else
        {
            return $result->getReal() . $result->getImaginary() . $result->getSuffix();
//            return sprintf('%.20f', floatval(abs(sprintf('%.20f', floatval($result->getReal())) . '+' .
//                sprintf('%.20f', floatval($result->getImaginary())) . $result->getSuffix())));
        }
    }

    /**
     * @param $imaginary
     *
     * @return int|string
     */
    private function calculateImaginaryValue ($imaginary)
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Get number of i in string
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach ( count_chars($imaginary, 1) as $i => $val )
        {
            $length = $val;
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Return i value
         *
         -------------------------------------------------------------------------------------------------------------*/
        if ($length % 4 === 1)
        {
            return 'i';
        }
        else
        {
            if ($length % 4 === 2)
            {
                return -1;
            }
            else
            {
                if ($length % 4 === 3)
                {
                    return '-i';
                }
                else
                {
                    if ($length % 4 === 0)
                    {
                        return 1;
                    }
                }
            }
        }
    }
}