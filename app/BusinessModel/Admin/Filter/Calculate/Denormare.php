<?php

namespace App\BusinessModel\Admin\Filter\Calculate;

/**
 * Class Denormare
 * @package App\BusinessModel\Admin\Filter\Calculate
 */
class Denormare
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @var
     */
    protected $denumitor;

    /**
     * @var
     */
    protected $numitor;

    /**
     * Denormare constructor.
     *
     * @param array $params
     */
    public function __construct (array $params)
    {
        $this->params = $params;
    }

    /**
     * @param int $order
     *
     * @return array
     */
    public function calculateDenormationChanges(int $order) : array
    {
        $values = [];
        for($i = 0; $i <= $order; $i++)
        {
            $values[] = pow(2 * pi() * $this->params['frecventa_de_trecere'], $i);
        }

        return $values;
    }

    /**
     * @param array $data
     */
    public function calculateFunction(array $data) : array
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate denumitor
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->calculateDenumitor($data);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate numitor
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->calculateNumitor($data);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Return response with denumitor & numitor
         *
         -------------------------------------------------------------------------------------------------------------*/
        return $this->formatUserFriendlyData();
    }

    /**
     * @param array $data
     *
     * @return array|int
     */
    private function calculateDenumitor(array $data)
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define power of s
         *
         -------------------------------------------------------------------------------------------------------------*/
        $valuesForPowerOfS = $this->resolvePowerOfSValues($data['denumitor']);

        $results = [];
        for($i = 0; $i <= $data['order']; $i++)
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Multiply first element from denumitor with the denormation If is greater than 0
             *
             ---------------------------------------------------------------------------------------------------------*/
            if(reset($data['denumitor']) > 0){
                $results[$i]['pow_off_s'] = $valuesForPowerOfS[$i];
                $results[$i]['value'] = reset($data['denumitor']) * reset($data['denormation_changed_values']);
            }

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Remove from array the elements that was multiplyed
             *
             ---------------------------------------------------------------------------------------------------------*/
            array_shift($data['denumitor']);
            array_shift($data['denormation_changed_values']);
        }

        $this->denumitor = $this->formatData($results);
    }

    /**
     * @param array $data
     */
    private function calculateNumitor(array $data)
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define power of s
         *
         -------------------------------------------------------------------------------------------------------------*/
        $valuesForPowerOfS = $this->resolvePowerOfSValues($data['numitor']);

        $results = [];
        for($i = 0; $i <= $data['order']; $i++)
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Multiply first element from denumitor with the
             *
             ---------------------------------------------------------------------------------------------------------*/
            $results[$i]['pow_off_s'] =  $valuesForPowerOfS[$i];
            $results[$i]['value'] = reset($data['numitor']) * reset($data['denormation_changed_values']);

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Remove from array the elements that was multiplyed
             *
             ---------------------------------------------------------------------------------------------------------*/
            array_shift($data['numitor']);
            array_shift($data['denormation_changed_values']);
        }

        $this->numitor = $this->formatData($results);
    }

    /**
     * @param array $values
     *
     * @return array
     */
    public function resolvePowerOfSValues($values) : array
    {
        $valuesForPowerOfS = [];
        foreach($values as $key => $value)
        {
            $valuesForPowerOfS[] = $key;
        }

        return $valuesForPowerOfS;
    }

    /**
     * @return array
     */
    private function formatUserFriendlyData() : array
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Format denumitor
         *
         -------------------------------------------------------------------------------------------------------------*/
        if(is_array($this->denumitor))
        {
            $formattedDen = [];
            foreach($this->denumitor as $den)
            {
                if(strlen($den['value']) >= 4)
                {
                    $formattedDen[] = [
                        'value' => $den['value'] / (pow(10, strlen(round($den['value'])) - 2)),
                        'pow'   => strlen(round($den['value'])) - 2,
                        'pow_off_s'   => $den['pow_off_s']
                    ];
                }
                else
                {
                    $formattedDen[] = [
                        'value' => $den['value'],
                        'pow'   => 0,
                        'pow_off_s'   => $den['pow_off_s']
                    ];
                }
            }
        }
        else
        {
            if(strlen($this->denumitor['value']) >= 4)
            {
                $formattedDen = [
                    'value' => $this->denumitor['value'] / (pow(10,strlen(round($this->denumitor['value'])) - 2)),
                    'pow'   => strlen(round($this->denumitor['value'])) - 2,
                    'pow_off_s'   => $this->denumitor['pow_off_s']
                ];
            }
            else
            {
                $formattedDen = [
                    'value' => $this->denumitor['value'],
                    'pow'   => 0,
                    'pow_off_s'   => $this->denumitor['pow_off_s']
                ];
            }
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Format numitor
         *
         -------------------------------------------------------------------------------------------------------------*/
        if(is_array($this->numitor))
        {
            $formattedNum = [];
            foreach($this->numitor as $num)
            {
                if(strlen($num['value']) >= 4)
                {
                    $formattedNum[] = [
                        'value' => $num ['value']/ (pow(10, strlen(round($num['value'])) - 2)),
                        'pow'   => strlen(round($num['value'])) - 2,
                        'pow_off_s'   => $num['pow_off_s']
                    ];
                }
                else
                {
                    $formattedNum[] = [
                        'value' => $num['value'],
                        'pow'   => 0,
                        'pow_off_s'   => $num['pow_off_s']
                    ];
                }
            }
        }
        else
        {
            if(strlen($this->numitor) >= 4)
            {
                $formattedNum = [
                    'value' => $this->numitor['value'] / (pow(10,strlen(round($this->numitor['value'])) - 2)),
                    'pow'   => strlen(round($this->numitor['value'])) - 2,
                    'pow_off_s'   => $this->numitor['pow_off_s']
                ];
            }
            else
            {
                $formattedNum = [
                    'value' => $this->numitor['value'],
                    'pow'   => 0,
                    'pow_off_s'   => $this->numitor['pow_off_s']
                ];
            }
        }

        return [
            'denumitor' => $formattedDen,
            'numitor'   => $formattedNum
        ];
    }

    /**
     * @param array $results
     *
     * @return array|int
     */
    private function formatData(array $results)
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Check if result has s values in it
         *
         -------------------------------------------------------------------------------------------------------------*/
        $isNumitorOne = false;
        $count = 0;
        foreach($results as $result)
        {
            /*---------------------------------------------------------------------------------------------------------10101
             *
             * If has at least 2 zeros
             *
             -------------------------------------------------------------------------------------------------------------*/
            if ($result['value'] == 0)
            {
                $count++;
            }
            if($count >= 2)
            {
                $isNumitorOne = true;
                break;
            }
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * If has s values, calculate in a way.
         * Else add numbers
         *
         -------------------------------------------------------------------------------------------------------------*/
        if($isNumitorOne)
        {
            $value = 0;
            foreach($results as $result)
            {
                $value += $result['value'];
            }
            return $value;
        }
        else
        {
            return $results;
        }
    }
}