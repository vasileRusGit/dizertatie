<?php

namespace App\BusinessModel\Admin\Filter\Calculate;

use Complex\Complex;

/**
 * Class PhaseAndAmplitude
 * @package App\BusinessModel\Admin\Filter\Calculate
 */
class PhaseAndAmplitude
{

    /**
     * @var mixed
     */
    protected $denumitor;

    /**
     * @var mixed
     */
    protected $numitor;

    /**
     * PhaseAndAmplitude constructor.
     *
     * @param $den
     * @param array $num
     */
    public function __construct ($den, array $num)
    {
        $this->denumitor = $den;
        $this->numitor = $num;
    }

    /**
     * @return array
     *
     */
    public function calculateAmplitude() : array
    {
        $results = [];
        foreach($this->prepareData() as $key => $value)
        {
            $items = $this->multiplyPolyValWithHisConjugate($value);

            /*---------------------------------------------------------------------------------------------------------10101
             *
             * Check if real value is zero
             *
             -------------------------------------------------------------------------------------------------------------*/
            if(isset($items['real'])){
                $realValue = $items['real'];
            }
            else{
                $realValue = 0;
            }

            $radicalValue = sqrt(pow($realValue, 2) +  pow($items['img'], 2));
            $results[$key] = 20 * log10($radicalValue);
        }
//print_r($results);die;
        return $results;
    }

    /**
     * @return array
     */
    public function calculatePhase() : array
    {
        $results = [];
        foreach($this->prepareData() as $key => $value)
        {
            $complex = new Complex($value);
            $imaginary = $complex->getImaginary();
            $real = $complex->getReal();

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if one of the values is 0
             *
             ---------------------------------------------------------------------------------------------------------*/
            if($imaginary == 0 || $real == 0)
            {
                $results[$key] = 0;
            }
            else
            {
                $results[$key] = atan2($imaginary, $real);
            }
        }

        return $results;
    }

    /**
     * @return array
     */
    public function prepareData() : array
    {
        $values = [];
        for ( $i = -1200; $i <= 1200; $i++ )
        {
            $value = '';
            foreach ( $this->numitor as $powerOfS => $valueOfS )
            {
                if($powerOfS > 0)
                {
//                    $value .= str_replace(',', '', pow($i * $valueOfS, $powerOfS)) . '*'.str_repeat("i", $powerOfS) . '+';
                    $value .= str_replace(',', '', number_format((float)pow(pow(10, $i / 100) * $valueOfS, $powerOfS), 50)) . '*'.str_repeat("i", $powerOfS) . '+';
                }
                else
                {
                    $value .= $valueOfS . '+';
                }
//                $value .= str_replace(',', '', number_format((float)pow(pow(10, $i / 100) * $valueOfS, $powerOfS), 50)) . '*'.str_repeat("i", $powerOfS) . '+';
            }
//print_r($value);die;
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if contain i
             *
             ---------------------------------------------------------------------------------------------------------*/
            $results = explode('+', $value);
            $replaceValues = [];
            foreach($results as $result)
            {
                if($result !== '')
                {
                    if(strpos($result, 'i') !== false)
                    {
                        /*-----------------------------------------------------------------------------------------10101
                         *
                         * Separate number from i values.
                         * We return pow of i values and multiply with the number
                         *
                         ---------------------------------------------------------------------------------------------*/
                        $separateRealAndIValues = explode('*', $result);
                        $realPartOfNumber = number_format((float)$separateRealAndIValues[0], 50);
                        $imaginaryPartOfNumber = $separateRealAndIValues[1];

                        /*-----------------------------------------------------------------------------------------10101
                         *
                         * Check if return of i calculation is number or string
                         *
                         ---------------------------------------------------------------------------------------------*/
                        $powOfIValue = $this->calculateImaginaryPowValue($imaginaryPartOfNumber);
                        if(strpos($powOfIValue, 'i') !== false)
                        {
                            $replaceValues['imaginary'][] = number_format((float)$realPartOfNumber, 50)  . '*' . $powOfIValue;
                        }
                        else
                        {
                            $replaceValues['real'][] = str_replace(',', '', $realPartOfNumber) * $powOfIValue;
                        }
                    }
                    else
                    {
                        $replaceValues['real'][] = str_replace('*', '', $result);
                    }
                }
            }

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Add real number and imaginary numbers individually
             *
             ---------------------------------------------------------------------------------------------------------*/
            $realValue = 0;
            $imaginaryValue = 0;
            foreach($replaceValues['real'] as $realNumber)
            {
                $realValue += str_replace(',', '', str_replace('*', '', number_format((float)$realNumber, 50)));
            }

            foreach($replaceValues['imaginary'] as $imaginaryNumber)
            {
                /*-------------------------------------------------------------------------------------------------10101
                 *
                 * If number contain - , striped out and multiply with -1
                 *
                 -----------------------------------------------------------------------------------------------------*/
                $value = str_replace('i', '', str_replace('*', '', $imaginaryNumber));
                if(strpos($value, '-') !== false)
                {
                    if(substr_count($value, '-') === 2)
                    {
                        $value = 1 * (float)str_replace('-', '', str_replace('-', '', $value));
                    }
                    else if(substr_count($value, '-') === 1)
                    {
                        $value = -1 * (float)str_replace('-', '', $value);
                    }
                }

                $imaginaryValue += number_format((float)$value, 50);
                if( !next($replaceValues['imaginary'])) {
                    if($imaginaryValue > 0)
                    {
                        $imaginaryValue = '+' . $imaginaryValue;
                    }
                    $imaginaryValue = $imaginaryValue . 'i';
                }
            }
            $finalResult = $realValue . $imaginaryValue;

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Calculate denumitor divided to numitor substituion
             *
             ---------------------------------------------------------------------------------------------------------*/
            $complex = new Complex($finalResult);
            $result = $complex->divideinto($this->denumitor);

            $formattedImaginaryValue = number_format($result->getImaginary(), 50);
            if(str_replace('i', '', $formattedImaginaryValue) > 0)
            {
                $formattedImaginaryValue = '+' . $formattedImaginaryValue;
            }
            $formattedRealValue = number_format($result->getReal(), 50);
            if($formattedRealValue > 0)
            {
                $formattedRealValue = '+' . $formattedRealValue;
            }

            /*---------------------------------------------------------------------------------------------------------10101
             *
             * We do not add the value calculated on $i = 0, we get wrong data
             *
             -------------------------------------------------------------------------------------------------------------*/
            if($i !== 0)
            {
                $values[$i] = str_replace(',', '', $formattedRealValue . $formattedImaginaryValue . $result->getSuffix());
            }
//
//            print_r($values[$i]);die;
        }

        return $values;
    }

    /**
     * @param string $value
     *
     * @return array
     */
    public function multiplyPolyValWithHisConjugate(string $value) : array
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * If has just +
         * Else if has just -
         * Else has a + and a -
         *
         -------------------------------------------------------------------------------------------------------------*/
        if(strpos($value, '+') !== false && strpos($value, '-') === false)
        {
            return $this->formatValuesForComplexConjugate($value, '+');
        }
        else if(strpos($value, '+') === false)
        {
            return $this->formatValuesForComplexConjugate($value, '-');
        }
        else if(strpos($value, '+') !== false && strpos($value, '-') !== false)
        {
            return $this->formatValuesForComplexConjugate($value, '+-');
        }
    }

    /**
     * @param string $values
     * @param string $sign
     *
     * @return array
     */
    public function formatValuesForComplexConjugate(string $values, string $sign) : array
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Set values sign
         *
         -------------------------------------------------------------------------------------------------------------*/
        if($sign === '+-')
        {
            $firstExplode = explode('+', $values);
            if(empty($firstExplode[0]))
            {
                $realNumberSign = 1;
                $imaginaryNumberSign = -1;
                $matches = explode('-', $firstExplode[1]);
                $matches[1] = '-' . $matches[1];
            }
            else
            {
                $realNumberSign = -1;
                $imaginaryNumberSign = 1;
                $matches = $firstExplode;
            }
        }
        else
        {
            if($sign === '+'){$realNumberSign = $imaginaryNumberSign = 1;}
            if($sign === '-'){$realNumberSign = $imaginaryNumberSign = -1;}
            $matches = explode($sign, $values);
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate variables
         *
         -------------------------------------------------------------------------------------------------------------*/
        $variables = [];
        foreach($matches as $match)
        {
            if(!empty($match))
            {
                if(strpos($match, 'i') !== false)
                {
                    $imaginaryValue = str_replace('i', '', str_replace('*', '',$match));
                    if(($imaginaryValue < 0 && $imaginaryNumberSign === 1) || ($imaginaryValue > 0 && $imaginaryNumberSign === -1))
                    {
                        $imaginaryValue = $imaginaryNumberSign * $imaginaryValue;
                    }
                    $variables['img'] = $imaginaryValue;
                }
                else
                {
                    $realValue = (float)str_replace('*', '',$match);
                    if(($realValue < 0 && $realNumberSign === 1) || ($realValue > 0 && $realNumberSign === -1))
                    {
                        $realValue = $realNumberSign * $realValue;
                    }
                    $variables['real'] = $realValue;
                }
            }
        }

        return $variables;
    }

    /**
     * @param $imaginary
     *
     * @return int|string
     */
    private function calculateImaginaryPowValue ($imaginary)
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Get number of i in string
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach ( count_chars($imaginary, 1) as $i => $val )
        {
            $length = $val;
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Return i value
         *
         -------------------------------------------------------------------------------------------------------------*/
        if ($length % 4 === 1)
        {
            return 'i';
        }
        else
        {
            if ($length % 4 === 2)
            {
                return -1;
            }
            else
            {
                if ($length % 4 === 3)
                {
                    return '-i';
                }
                else
                {
                    if ($length % 4 === 0)
                    {
                        return 1;
                    }
                }
            }
        }
    }
}