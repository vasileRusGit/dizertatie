<?php

namespace App\BusinessModel\Admin\Filter\Design\Butterworth;

use App\BusinessModel\Admin\Filter\Calculate\Denormare;
use App\BusinessModel\Admin\Filter\Calculate\Order;
use App\BusinessModel\Admin\Filter\Calculate\Polls;
use App\BusinessModel\Admin\Filter\Calculate\PolyVal;
use App\BusinessModel\Admin\Filter\Calculate\PhaseAndAmplitude as PhaseAndAmplitudeBusinessModel;
use Illuminate\Http\Request;

/**
 * Class FtjFts
 *
 * @package App\BusinessModel\Admin\Filter\Design\Butterworth
 */
class FtjFts
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var int
     */
    protected $order;

    /**
     * @var array
     */
    protected $polls;

    /**
     * @var array
     */
    protected $transferFunctionCoefficients;

    /**
     * @var array
     */
    protected $denormareOfTransferFunction;

    /**
     * @var array
     */
    protected $polyValFunctionValues;

    /**
     * @var
     */
    protected $phaseAndAmplitude;

    /**
     * @var Polls
     */
    protected $pollsCalculationBusinessModel;

    /**
     * @var Order
     */
    protected $orderCalculationBusinessModel;

    /**
     * @var Denormare
     */
    protected $denormareCalculationBusinessModel;

    /**
     * @var PolyVal
     */
    protected $polyValCalculationBusinessModel;

    /**
     * @var PhaseAndAmplitudeBusinessModel
     */
    protected $phaseAndAmplitudeBusinessModel;

    /**
     * FtjFts constructor.
     *
     * @param array $params
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
        $this->orderCalculationBusinessModel = new Order($request->params);
        $this->denormareCalculationBusinessModel = new Denormare($request->params);
    }

    /**
     *
     */
    public function resolve ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate order and polls
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->order = $this->orderCalculationBusinessModel->calculateOrder();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Instantiate calculate polls business model
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->pollsCalculationBusinessModel = new Polls($this->order);
        $this->polls = $this->pollsCalculationBusinessModel->calculatePols();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * If Filter Type is FTJ, Denormarea
         * Else FTS Transformare
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->transferFunctionCoefficients = $this->pollsCalculationBusinessModel->calculateTransferFunctionCoefficients();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Create denormation array (s->s/(2*pi*fpass)2)
         *
         -------------------------------------------------------------------------------------------------------------*/
        $temp = $this->transferFunctionCoefficients;
        krsort($temp);
        $num = $temp;
        $denormationChangedValues = $this->denormareCalculationBusinessModel->calculateDenormationChanges($this->order);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate denormata functiei de transfer
         *
         -------------------------------------------------------------------------------------------------------------*/
        $den = [];
        // add zeros foreach s coefficient but s0
        for ($i = $this->order; $i > 0; $i--)
        {
            $den[$i] = 0;
        }
        $den[0] = 1;

        // prepare data
        $data = [
            'order' => $this->order,
            'denumitor' => $den,
            'numitor' => $num,
            'denormation_changed_values' => $denormationChangedValues
        ];

        $this->denormareOfTransferFunction = $this->denormareCalculationBusinessModel->calculateFunction($data);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate amplitude & phase values of the transfer function
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->phaseAndAmplitudeBusinessModel = new PhaseAndAmplitudeBusinessModel(1,
            $this->transferFunctionCoefficients);
        $this->phaseAndAmplitude['transfer_function']['amplitude'] = $this->phaseAndAmplitudeBusinessModel->calculateAmplitude();
        $this->phaseAndAmplitude['transfer_function']['phase'] = $this->phaseAndAmplitudeBusinessModel->calculatePhase();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Calculate amplitude & phase values of the denormation function
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->phaseAndAmplitudeBusinessModel = new PhaseAndAmplitudeBusinessModel($this->formatDenCoefficients()['denumitor'],
            $this->formatDenCoefficients()['numitor']);
        $this->phaseAndAmplitude['denormation_function']['amplitude'] = $this->phaseAndAmplitudeBusinessModel->calculateAmplitude();
        $this->phaseAndAmplitude['denormation_function']['phase'] = $this->phaseAndAmplitudeBusinessModel->calculatePhase();
    }

    /**
     * @return array
     */
    protected function formatDenCoefficients() : array
    {
        $values = [];
        $denArray = [];
        $numArray = [];
        foreach($this->denormareOfTransferFunction['denumitor'] as $den)
        {
            $denArray[$den['pow_off_s']] = $den['value'] * pow(10, $den['pow']);
        }

        foreach($this->denormareOfTransferFunction['numitor'] as $num)
        {
            $numArray[$num['pow_off_s']] = $num['value'] * pow(10, $num['pow']);
        }
        $values['denumitor'] = $denArray;
        $values['numitor'] = $numArray;

        return $values;
    }

    /**
     * @return array
     */
    public function getPolls() : array
    {
        return $this->polls;
    }

    /**
     * @return array
     */
    public function getTransferFunctionCoefficients() : array
    {
        return $this->transferFunctionCoefficients;
    }

    /**
     * @return array
     */
    public function getDenormationTransferFunction() : array
    {
        return $this->denormareOfTransferFunction;
    }

    /**
     * @return array
     */
    public function getPhaseAndAmplitudeValues() : array
    {
        return $this->phaseAndAmplitude;
    }
}