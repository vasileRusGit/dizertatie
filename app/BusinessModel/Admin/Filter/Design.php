<?php

namespace App\BusinessModel\Admin\Filter;

use Illuminate\Http\Request;
use App\BusinessModel\Admin\Filter\Design\Butterworth\FtjFts as ButterworthFtjFtsFilterBusinessModel;

/**
 * Class Design
 *
 * @package App\BusinessModel\Admin\Filter
 */
class Design
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var array
     */
    protected $polls;

    /**
     * @var
     */
    protected $filterBusinessModel;

    /**
     * Design constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
        $this->filterBusinessModel = new ButterworthFtjFtsFilterBusinessModel($request);
    }

    /**
     * @return mixed
     */
    public function design()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Check for filters
         *
         -------------------------------------------------------------------------------------------------------------*/
        if ($this->request->input('filter') === 'Butterworth')
        {
            if ($this->request->input('filterType') === 'FTJ' || $this->request->input('filterType') === 'FTS')
            {
                $this->filterBusinessModel->resolve();
            }
        }
    }

    /**
     * @return array
     */
    public function composePolls() : array
    {
        $formattedPolls = [];
        foreach($this->filterBusinessModel->getPolls() as $poll)
        {
            unset($poll['s']);
            $poll['img'] = str_replace('i', '', $poll['img']);
            $formattedPolls[] = $poll;
        }

        return $formattedPolls;
    }

    /**
     * @return array
     */
    public function composeTransferFunctionCoefficients() : array
    {
        $formatted = [];
        $coefficients = $this->filterBusinessModel->getTransferFunctionCoefficients();
        foreach($coefficients as $key => $value)
        {
            $newKey = count($coefficients) - 1 - $key;
            $formatted[$newKey]['pow'] = $key;
            $formatted[$newKey]['value'] = $value;
        }

        return $formatted;
    }

    /**
     * @return array
     */
    public function composeDenormationTransferFunction() : array
    {
        return $this->filterBusinessModel->getDenormationTransferFunction();
    }

    /**
     * @return array
     */
    public function composePhaseAndAmplitudeValues() : array
    {
        $results = [];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Prepare transfer function amplitude data
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach($this->filterBusinessModel->getPhaseAndAmplitudeValues()['transfer_function']['amplitude'] as $key => $value)
        {
            $results['transfer_function']['amplitude']['values'][] = $value;
        }
        $results['transfer_function']['amplitude']['labels'] = $this->formatLabels(
            $this->filterBusinessModel->getPhaseAndAmplitudeValues()['transfer_function']['amplitude']);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Prepare transfer function phase data
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach($this->filterBusinessModel->getPhaseAndAmplitudeValues()['transfer_function']['phase'] as $key => $value)
        {
            $results['transfer_function']['phase']['values'][] = $value;
        }
        $results['transfer_function']['phase']['labels'] = $this->formatLabels(
            $this->filterBusinessModel->getPhaseAndAmplitudeValues()['transfer_function']['phase']);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Prepare transfer function amplitude data
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach($this->filterBusinessModel->getPhaseAndAmplitudeValues()['denormation_function']['amplitude'] as $key => $value)
        {
            $results['denormation_function']['amplitude']['values'][] = $value;
        }
        $results['denormation_function']['amplitude']['labels'] = $this->formatLabels(
            $this->filterBusinessModel->getPhaseAndAmplitudeValues()['denormation_function']['amplitude']);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Prepare transfer function phase data
         *
         -------------------------------------------------------------------------------------------------------------*/
        foreach($this->filterBusinessModel->getPhaseAndAmplitudeValues()['denormation_function']['phase'] as $key => $value)
        {
            $results['denormation_function']['phase']['values'][] = $value;
        }
        $results['denormation_function']['phase']['labels'] = $this->formatLabels(
            $this->filterBusinessModel->getPhaseAndAmplitudeValues()['denormation_function']['phase']);

        return $results;
    }

    /**
     * @param array $items
     *
     * @return array
     */
    protected function formatLabels(array $items) : array
    {
        $labels = [];
        $counter = 0;
        foreach(array_keys($items) as $key)
        {
            // first
            if($key == key($items))
            {
                $labels[$counter] = '0.01';
            }
            // middle
            else if($key == round((key(array_slice($items, -1, 1, true)) + key($items)) / 2))
            {
                $labels[$counter] = '0.1';
            }
            // last
            else if($key == key(array_slice($items, -1, 1, true)))
            {
                $labels[$counter] = '1';
            }
            else
            {
                $labels[$counter] = '';
            }
            $counter++;
        }

        return $labels;
    }
}