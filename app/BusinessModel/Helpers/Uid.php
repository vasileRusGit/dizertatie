<?php

namespace App\BusinessModel\Helpers;

use Illuminate\Support\Str;

/**
 * Class Uid
 *
 * @package App\BusinessModel\Helpers
 */
class Uid
{
    /**
     * Return a unique uid
     *
     * @return string
     */
    public static function generate()
    {
        return (string) Str::uuid();
    }
}