<?php

namespace App\BusinessModel\Helpers\Filter;

use Illuminate\Http\Request;

/**
 * Class Validate
 *
 * @package App\BusinessModel\Helpers\Filter
 */
class Validate
{
    /**
     * Validate constructor.
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return bool
     */
    public function validate () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Initial validate
         *
         -------------------------------------------------------------------------------------------------------------*/
        $requestValidation = $this->request->validate([
            'filter'     => 'required',
            'filterType' => 'required',
            'params'     => 'required',
        ]);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Check if request has filter, filter type and the list of params
         *
         -------------------------------------------------------------------------------------------------------------*/
        if ($requestValidation)
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check for filters
             *
             ---------------------------------------------------------------------------------------------------------*/
            if ($this->request->input('filter') === 'Butterworth')
            {
                if ($this->request->input('filterType') === 'FTJ' || $this->request->input('filterType') === 'FTS')
                {
                    return $this->validateButterworthFTJFTS();
                }
                return $this->validateButterworthFOBFTB();
            }
            elseif ($this->request->input('filter') === 'Chebisev')
            {
                if ($this->request->input('filterType') === 'FTJ' || $this->request->input('filterType') === 'FTS')
                {
                    return $this->validateChebisevFTJFTS();
                }
                return $this->validateChebisevFOBFTB();
            }
            elseif ($this->request->input('filter') === 'Invers-Chebisev')
            {
                if ($this->request->input('filterType') === 'FTJ' || $this->request->input('filterType') === 'FTS')
                {
                    return $this->validateInversChebisevFTJFTS();
                }
                return $this->validateInversChebisevFOBFTB();
            }
            elseif ($this->request->input('filter') === 'Cauer')
            {
                if ($this->request->input('filterType') === 'FTJ' || $this->request->input('filterType') === 'FTS')
                {
                    return $this->validateCauerFTJFTS();
                }
                return $this->validateCauerFOBFTB();
            }
        }
    }

    /**
     * Validate Butterworth FTJ / FTS
     *
     * @return bool
     */
    public function validateButterworthFTJFTS () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere'          => 'required',
            'params.frecventa_de_oprire'           => 'required',
            'params.atenuarea_in_banda_de_trecere' => 'required',
            'params.atenuarea_in_banda_de_oprire'  => 'required'
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere.required'          => 'The Frecventa de trecere field is required',
            'params.frecventa_de_oprire.required'           => 'The Frecventa de oprire field is required',
            'params.atenuarea_in_banda_de_trecere.required' => 'The Atenuarea in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire.required'  => 'The Atenuarea in banda de oprire field is required'
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }

    /**
     * Validate Butterworth FOB / FTB
     *
     * @return bool
     */
    public function validateButterworthFOBFTB () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere_inferioara' => 'required',
            'params.frecventa_de_trecere_superioara' => 'required',
            'params.frecventa_de_oprire_inferioara'  => 'required',
            'params.frecventa_de_oprire_superioara'  => 'required',
            'params.atenuarea_in_banda_de_trecere'   => 'required',
            'params.atenuarea_in_banda_de_oprire'    => 'required'
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere_inferioara.required' => 'The Frecventa de trecere inferioara field is required',
            'params.frecventa_de_trecere_superioara.required' => 'The Frecventa de trecere superioara field is required',
            'params.frecventa_de_oprire_inferioara.required'  => 'The Frecventa de oprire inferioara field is required',
            'params.frecventa_de_oprire_superioara.required'  => 'The Frecventa de oprire superioara field is required',
            'params.atenuarea_in_banda_de_trecere.required'   => 'The Atenuarea in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire.required'    => 'The Atenuarea in banda de oprire field is required'
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }

    /**
     * Validate Chebisev FTJ / FTS
     *
     * @return bool
     */
    public function validateChebisevFTJFTS () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere'              => 'required',
            'params.frecventa_de_oprire'               => 'required',
            'params.atenuarea_in_banda_de_trecere'     => 'required',
            'params.atenuarea_in_banda_de_oprire'      => 'required',
            'params.riplul_permis_in_banda_de_trecere' => 'required'
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere.required'          => 'The Frecventa de trecere field is required',
            'params.frecventa_de_oprire.required'           => 'The Frecventa de oprire field is required',
            'params.atenuarea_in_banda_de_trecere.required' => 'The Atenuarea in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire.required'  => 'The Atenuarea in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_trecere'      => 'The Riplul permis in banda de trecere field is required',
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }

    /**
     * Validate Chebisev FOB / FTB
     *
     * @return bool
     */
    public function validateChebisevFOBFTB () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere_inferioara'   => 'required',
            'params.frecventa_de_trecere_superioara'   => 'required',
            'params.frecventa_de_oprire_inferioara'    => 'required',
            'params.frecventa_de_oprire_superioara'    => 'required',
            'params.atenuarea_in_banda_de_trecere'     => 'required',
            'params.atenuarea_in_banda_de_oprire'      => 'required',
            'params.riplul_permis_in_banda_de_trecere' => 'required'
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere_inferioara'   => 'The Frecventa de trecere inferioara field is required',
            'params.frecventa_de_trecere_superioara'   => 'The Frecventa de trecere superioara field is required',
            'params.frecventa_de_oprire_inferioara'    => 'The Frecventa de oprire inferioara field is required',
            'params.frecventa_de_oprire_superioara'    => 'The Frecventa de oprire superioara field is required',
            'params.atenuarea_in_banda_de_trecere'     => 'The Frecventa in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire'      => 'The Frecventa in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_trecere' => 'The Riplul permis in banda de trecere field is required'
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }

    /**
     * Validate Invers-Chebisev FTJ / FTS
     *
     * @return bool
     */
    public function validateInversChebisevFTJFTS () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere'             => 'required',
            'params.frecventa_de_oprire'              => 'required',
            'params.atenuarea_in_banda_de_trecere'    => 'required',
            'params.atenuarea_in_banda_de_oprire'     => 'required',
            'params.riplul_permis_in_banda_de_oprire' => 'required'
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere.required'          => 'The Frecventa de trecere field is required',
            'params.frecventa_de_oprire.required'           => 'The Frecventa de oprire field is required',
            'params.atenuarea_in_banda_de_trecere.required' => 'The Atenuarea in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire.required'  => 'The Atenuarea in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_oprire'       => 'The Riplul permis in banda de trecere field is required'
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }

    /**
     * Validate Invers-Chebisev FOB / FTB
     *
     * @return bool
     */
    public function validateInversChebisevFOBFTB () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere_inferioara'   => 'required',
            'params.frecventa_de_trecere_superioara'   => 'required',
            'params.frecventa_de_oprire_inferioara'    => 'required',
            'params.frecventa_de_oprire_superioara'    => 'required',
            'params.atenuarea_in_banda_de_trecere'     => 'required',
            'params.atenuarea_in_banda_de_oprire'      => 'required',
            'params.riplul_permis_in_banda_de_oprire'  => 'required',
            'params.riplul_permis_in_banda_de_trecere' => 'required'
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere_inferioara.required' => 'The Frecventa de trecere inferioara field is required',
            'params.frecventa_de_trecere_superioara.required' => 'The Frecventa de trecere superioara field is required',
            'params.frecventa_de_oprire_inferioara.required'  => 'The Frecventa de oprire inferioara field is required',
            'params.frecventa_de_oprire_superioara.required'  => 'The Frecventa de oprire superioara field is required',
            'params.atenuarea_in_banda_de_trecere.required'   => 'The Atenuarea in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire.required'    => 'The Atenuarea in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_oprire'         => 'The Riplul permis in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_trecere'        => 'The Riplul permis in banda de trecere field is required'
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }

    /**
     * Validate Cauer FTJ / FTS
     *
     * @return bool
     */
    public function validateCauerFTJFTS () : bool
    {
//        print_r($this->request->params);die;
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere'             => 'required',
            'params.frecventa_de_oprire'              => 'required',
            'params.atenuarea_in_banda_de_trecere'    => 'required',
            'params.atenuarea_in_banda_de_oprire'     => 'required',
            'params.riplul_permis_in_banda_de_oprire' => 'required',
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere.required'          => 'The Frecventa de trecere field is required',
            'params.frecventa_de_oprire.required'           => 'The Frecventa de oprire field is required',
            'params.atenuarea_in_banda_de_trecere.required' => 'The Atenuarea in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire.required'  => 'The Atenuarea in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_oprire'       => 'The Riplul permis in banda de oprire field is required',
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }

    /**
     * Validate Cauer FOB / FTB
     *
     * @return bool
     */
    public function validateCauerFOBFTB () : bool
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define rules
         *
         -------------------------------------------------------------------------------------------------------------*/
        $rules = [
            'params.frecventa_de_trecere_inferioara'   => 'required',
            'params.frecventa_de_trecere_superioara'   => 'required',
            'params.frecventa_de_oprire_inferioara'    => 'required',
            'params.frecventa_de_oprire_superioara'    => 'required',
            'params.atenuarea_in_banda_de_trecere'     => 'required',
            'params.atenuarea_in_banda_de_oprire'      => 'required',
            'params.riplul_permis_in_banda_de_oprire'  => 'required',
            'params.riplul_permis_in_banda_de_trecere' => 'required'
        ];

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define messages
         *
         -------------------------------------------------------------------------------------------------------------*/
        $messages = [
            'params.frecventa_de_trecere_inferioara.required' => 'The Frecventa de trecere inferioara field is required',
            'params.frecventa_de_trecere_superioara.required' => 'The Frecventa de trecere superioara field is required',
            'params.frecventa_de_oprire_inferioara.required'  => 'The Frecventa de oprire inferioara field is required',
            'params.frecventa_de_oprire_superioara.required'  => 'The Frecventa de oprire superioara field is required',
            'params.atenuarea_in_banda_de_trecere.required'   => 'The Atenuarea in banda de trecere field is required',
            'params.atenuarea_in_banda_de_oprire.required'    => 'The Atenuarea in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_oprire'         => 'The Riplul permis in banda de oprire field is required',
            'params.riplul_permis_in_banda_de_trecere'        => 'The Riplul permis in banda de trecere field is required'
        ];

        if($this->request->validate($rules, $messages))
        {
            return true;
        }

        return false;
    }
}