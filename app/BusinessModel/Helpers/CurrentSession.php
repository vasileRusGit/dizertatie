<?php

namespace App\BusinessModel\Helpers;

use App\Http\Middleware\Auth\Check;
use App\Models\User;

/**
 * Class CurrentSession
 *
 * @package App\BusinessModel\Helpers
 */
class CurrentSession extends Check
{
    /**
     * @return User
     */
    public static function getUser() : User
    {
        return parent::$user;
    }
}