<?php

namespace App\Console\Commands\Bootstrap;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

/**
 * Class Init
 *
 * @package App\Console\Commands\Bootstrap
 */
class Init extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bootstrap:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Setting up progress bar
         *
         -------------------------------------------------------------------------------------------------------------*/
        $bar = $this->output->createProgressBar(19);
        $bar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%');

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Refreshing database tables
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->info("Clear app cache");
        Artisan::call('cache:clear');
        Artisan::call('config:cache');
        Artisan::call('config:clear');
        $bar->advance();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Refreshing database tables
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->info("\nRefreshing database tables");
        Artisan::call('migrate:refresh');
        $bar->advance();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Creating student and professor users
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->info("\nCreating student and professor account");
        Artisan::call('generate:users');
        $bar->advance();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Finish progress bar
         *
         -------------------------------------------------------------------------------------------------------------*/
        $bar->finish();
    }
}
