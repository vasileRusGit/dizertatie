<?php

namespace App\Console\Commands\Generate;

use App\BusinessModel\Helpers\Uid;
use App\Models\User;
use Hash;
use Illuminate\Console\Command;

class Users extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Creating a student user account
         *
         -------------------------------------------------------------------------------------------------------------*/
        $student = new User();
        $student->uid = Uid::generate();
        $student->first_name = 'Vasile';
        $student->last_name = 'Rus';
        $student->email = 'student@dizertatie.ro';
        $student->password = Hash::make('utcn');
        $student->description = 'This is a student account.';
        $student->image_path = 'img/no_image_avatar.png';
        $student->save();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Creating a professor user account
         *
         -------------------------------------------------------------------------------------------------------------*/
        $professor = new User();
        $professor->uid = Uid::generate();
        $professor->first_name = 'Botond';
        $professor->last_name = 'Kirei';
        $professor->email = 'profesor@dizertatie.ro';
        $professor->password = Hash::make('utcn');
        $professor->description = 'This is a professor account.';
        $professor->image_path = 'img/no_image_avatar.png';
        $professor->save();
    }
}
