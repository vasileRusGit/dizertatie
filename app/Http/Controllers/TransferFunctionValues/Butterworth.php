<?php

namespace App\Http\Controllers\TransferFunctionValues;

use App\BusinessModel\Admin\Filter\Calculate\Polls;
use App\Configuration\Response;
use Illuminate\Http\Request;

/**
 * Class Butterworth
 * @package App\Http\Controllers\TransferFunctionValues
 */
class Butterworth
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Polls
     */
    protected $pollsCalculationBusinessModel;

    /**
     * Butterworth constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     *
     */
    public function resolve ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Define initial array
         *
         -------------------------------------------------------------------------------------------------------------*/
        $transferFunctionValues = [
            [
                's0' => '1',
                's1' => '1'
            ],
            [
                's0' => '1',
                's1' => '1.414214',
                's2' => '1'
            ]
        ];

        for ( $i = 3; $i <= 10; $i++ )
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Instantiate calculate polls business model
             *
             ---------------------------------------------------------------------------------------------------------*/
            $this->pollsCalculationBusinessModel = new Polls($i);
            $this->polls = $this->pollsCalculationBusinessModel->calculatePols();

            $data = [];
            foreach($this->pollsCalculationBusinessModel->calculateTransferFunctionCoefficients() as $key => $coefficient)
            {
                $data['s' . $key] = $coefficient;
            }
            $transferFunctionValues[] = $data;
        }

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Return response
         *
         -------------------------------------------------------------------------------------------------------------*/

        return response()->json([
            'code' => Response::GENERAL_SUCCESS,
            'data' => $transferFunctionValues
        ]);
    }
}