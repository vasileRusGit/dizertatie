<?php

namespace App\Http\Controllers\Filter;

use App\BusinessModel\Helpers\CurrentSession;
use App\BusinessModel\Helpers\Uid;
use App\Configuration\Response;
use App\Models\Filter\Chart;
use Illuminate\Http\Request;

/**
 * Class Save
 * @package App\Http\Controllers\Filter
 */
class Save
{
    /**
     * Save constructor.
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    public function resolve()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Validate request
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->request->validate([
            'chartData'     => 'required',
            'name'          => 'required'
        ]);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Save filter chart
         *
         -------------------------------------------------------------------------------------------------------------*/
        $filterChart = new Chart();
        $filterChart->uid = Uid::generate();
        $filterChart->user_id = CurrentSession::getUser()->id;
        $filterChart->name = $this->request->name;
        $filterChart->rows = json_encode($this->request->chartData['rows']);
        $filterChart->columns = json_encode($this->request->chartData['columns']);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Save chart and return response
         *
         -------------------------------------------------------------------------------------------------------------*/
        if($filterChart->save())
        {
            return $this->responseSuccess();
        }

        return $this->responseFail();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json(['code' => Response::GENERAL_SUCCESS]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseFail()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json(['code' => Response::GENERAL_FAIL]);
    }
}