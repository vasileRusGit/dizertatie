<?php

namespace App\Http\Controllers\Filter;

use App\Configuration\Response;
use App\Models\Filter\Chart;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * Class Delete
 *
 * @package App\Http\Controllers\Filter
 */
class Delete
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolve ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Check if is quick action delete
         *
         -------------------------------------------------------------------------------------------------------------*/
        if($this->is_multi($this->request->all()))
        {
            foreach($this->request->all() as $item)
            {
                $this->deleteItem($item['uid']);
            }

            return $this->responseSuccess();
        }
        else
        {
            return $this->deleteItem($this->request->uid);
        }
    }

    /**
     * @param string $uid
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteItem(string $uid)
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Find filters
         *
         -------------------------------------------------------------------------------------------------------------*/
        try
        {
            $filter = Chart::findByUid($uid);

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Delete filter
             *
             ---------------------------------------------------------------------------------------------------------*/
            if($filter->delete())
            {
                return $this->responseSuccess();
            }

            return $this->responseFail();
        }
        catch(ModelNotFoundException $e)
        {
            return $this->responseFail();
        }
    }

    /**
     * @param $array
     *
     * @return bool
     */
    public function is_multi($array) {
        return (count($array) != count($array, 1));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json(['code' => Response::GENERAL_SUCCESS]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseFail ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json(['code' => Response::GENERAL_FAIL]);
    }
}