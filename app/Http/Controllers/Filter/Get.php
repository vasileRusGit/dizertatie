<?php

namespace App\Http\Controllers\Filter;

use App\BusinessModel\Helpers\CurrentSession;
use App\Configuration\Response;
use App\Models\Filter\Chart;
use Illuminate\Http\Request;

/**
 * Class Get
 * @package App\Http\Controllers\Filter
 */
class Get
{
    /**
     * @var
     */
    protected $filters;

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolve ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Find filters
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->filters = Chart::findByUser(CurrentSession::getUser());

        if ($this->filters)
        {
            return $this->responseSuccess();
        }

        return $this->responseFail();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json([
            'code' => Response::GENERAL_SUCCESS,
            'data' => $this->filters
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseFail ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json(['code' => Response::GENERAL_FAIL]);
    }
}