<?php

namespace App\Http\Controllers\Filter;

use App\BusinessModel\Helpers\Filter\Validate as ValidateBusinessModel;
use App\BusinessModel\Admin\Filter\Design as DesignBusinessModel;
use Illuminate\Http\Request;
use App\Configuration\Response;

/**
 * Class Design
 *
 * @package App\Http\Controllers\Filter
 */
class Design
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var DesignBusinessModel
     */
    protected $designBusinessModel;

    /**
     * @var ValidateBusinessModel
     */
    protected $validateBusinessModel;

    /**
     * Design constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
        $this->designBusinessModel = new DesignBusinessModel($request);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolve ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Validate request
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->validateBusinessModel = new ValidateBusinessModel($this->request);

        if($this->validateBusinessModel->validate())
        {
            /*------------------------------------------------------------------------------------------------------10101
             *
             * Instantiate design filter business model
             *
             ---------------------------------------------------------------------------------------------------------*/
            $this->designBusinessModel->design();

            return $this->responseSuccess();
        }

        return $this->responseFail();
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json([
            'code' => Response::GENERAL_SUCCESS,
            'polls' => $this->designBusinessModel->composePolls(),
            'transfer_function_coefficients' => $this->designBusinessModel->composeTransferFunctionCoefficients(),
            'denormation_transfer_function' => $this->designBusinessModel->composeDenormationTransferFunction(),
            'phase_and_amplitude_values' => $this->designBusinessModel->composePhaseAndAmplitudeValues()
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseFail()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Error occurred trying to design the filter
         *
         --------------------------------------------------------------------------------------------------------------*/
        return response()->json(['code' => Response::GENERAL_FAIL]);
    }
}