<?php

namespace App\Http\Controllers\Admin\User;

use App\Configuration\Response;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * Class Update
 *
 * @package App\Http\Controllers\Admin\User
 */
class Update
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * Get constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolve()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Validate request
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->validateRequest();

        try
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Get customer by uid
             *
             ---------------------------------------------------------------------------------------------------------*/
            $user = User::findByUid($this->request->header('Customer-Uid'));

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Update user
             *
             ---------------------------------------------------------------------------------------------------------*/
            $user->first_name = $this->request->input('first_name');
            $user->last_name = $this->request->input('last_name');
            $user->email = $this->request->input('email');
            // check if descriptions is set
            if ( $this->request->input('description'))
            {
                $user->description = $this->request->input('description');
            }
            // check if descriptions is set
            if ( $this->request->input('password'))
            {
                $user->password = \Hash::make($this->request->input('password'));
            }
            $user->save();

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * User was updated successfully
             *
             ---------------------------------------------------------------------------------------------------------*/
            return response()->json([
                'code' => Response::GENERAL_SUCCESS,
                'data' => $user->toArray()
            ]);
        }
        catch(ModelNotFoundException $e)
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Error occurred trying to update the user
             *
             ---------------------------------------------------------------------------------------------------------*/
            return response()->json([
                'code' => Response::GENERAL_FAIL
            ]);
        }
    }

    /**
     *
     */
    public function validateRequest()
    {
        if ($this->request->input('password'))
        {
            $this->request->validate([
                'first_name'            => 'required|string',
                'last_name'             => 'required|string',
                'email'                 => 'required|email',
                'password'              => 'required|confirmed|min:4',
                'password_confirmation' => 'required|min:4'
            ]);
        }
        else
        {
            $this->request->validate([
                'first_name'            => 'required|string',
                'last_name'             => 'required|string',
                'email'                 => 'required|email',
            ]);
        }
    }
}