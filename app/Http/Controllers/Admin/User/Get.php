<?php

namespace App\Http\Controllers\Admin\User;

use App\Configuration\Response;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * Class Get
 *
 * @package App\Http\Controllers\Admin\User
 */
class Get
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * Get constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resolve()
    {
        try
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if customer exist and has valid token
             *
             ---------------------------------------------------------------------------------------------------------*/
            $user = User::findByUid($this->request->header('Customer-Uid'));

            return response()->json([
                'code' => Response::GENERAL_SUCCESS,
                'data' => $user->toArray()
            ]);
        }
        catch(ModelNotFoundException $e)
        {
            return response()->json([
                'code' => Response::GENERAL_FAIL
            ]);
        }
    }
}