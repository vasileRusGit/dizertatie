<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Configuration\Response;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\BusinessModel\Admin\Auth\Token as TokenBusinessModel;

class Login
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var TokenBusinessModel
     */
    protected $tokenBusinessModel;

    /**
     * Register constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     *
     */
    public function resolve ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Validate request
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->request->validate([
            'email'                 => 'required',
            'password'              => 'required',
        ]);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Check if customer exist and has valid token
         *
         -------------------------------------------------------------------------------------------------------------*/
        try
        {
            $user = User::findByEmail($this->request->input('email'));

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if customer token it is belonging to user
             *
             ---------------------------------------------------------------------------------------------------------*/
            if(\Hash::check($this->request->input('password'), $user->password))
            {
                $this->tokenBusinessModel = new TokenBusinessModel($user);
                if ($this->tokenBusinessModel->resolve())
                {
                    /*---------------------------------------------------------------------------------------------10101
                     *
                     * Prepare user to be returned to frontend as array
                     *
                     -------------------------------------------------------------------------------------------------*/
                    $user->token = $this->tokenBusinessModel->getPlainToken();
                    $frontUser = $user->toArray();
                    unset($frontUser['access_token']);

                    /*---------------------------------------------------------------------------------------------10101
                     *
                     * Instantiate taken business model for creating a new auth token.
                     *
                     -------------------------------------------------------------------------------------------------*/
                    return response()->json([
                        'code'    => Response::GENERAL_SUCCESS,
                        'data'    => $frontUser
                    ]);
                }
            }

            return response()->json([
                'code' => Response::GENERAL_FAIL,
                'message' => 'Email or password are incorrect.'
            ]);
        }
        catch(ModelNotFoundException $e)
        {
            return response()->json([
                'code' => Response::GENERAL_FAIL,
                'message' => 'Email or password are incorrect.'
            ]);
        }
    }
}