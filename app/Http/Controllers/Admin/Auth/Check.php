<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Configuration\Response;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class Check
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * Register constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     *
     */
    public function resolve ()
    {
        try
        {
            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if customer exist and has valid token
             *
             ---------------------------------------------------------------------------------------------------------*/
            $user = User::findByUid($this->request->header('Customer-Uid'));

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if customer token it is belonging to user
             *
             ---------------------------------------------------------------------------------------------------------*/
            if(\Hash::check($this->request->header('Customer-token'), $user->accessToken->token))
            {
                return response()->json([
                    'code'    => Response::GENERAL_SUCCESS,
                ]);
            }

            return response()->json([
                'code' => Response::GENERAL_FAIL
            ]);
        }
        catch(ModelNotFoundException $e)
        {
            return response()->json([
                'code' => Response::GENERAL_FAIL
            ]);
        }
    }
}