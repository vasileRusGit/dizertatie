<?php

namespace App\Http\Controllers\Admin\Auth;

use App\BusinessModel\Admin\Auth\Token as TokenBusinessModel;
use App\BusinessModel\Helpers\Uid;
use App\Configuration\Response;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

/**
 * Class Register
 *
 * @package App\Http\Controllers\Admin\Auth
 */
class Register
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var TokenBusinessModel
     */
    protected $tokenBusinessModel;

    /**
     * Register constructor.
     *
     * @param Request $request
     */
    public function __construct (Request $request)
    {
        $this->request = $request;
    }

    /**
     *
     */
    public function resolve ()
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Validate request
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->request->validate([
            'first_name'            => 'required|string',
            'last_name'             => 'required|string',
            'email'                 => 'required|email|unique:users',
            'password'              => 'required|confirmed|min:4',
            'password_confirmation' => 'required|min:4'
        ]);

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Create a new user
         *
         -------------------------------------------------------------------------------------------------------------*/
        $user = new User();
        $user->uid = Uid::generate();
        $user->first_name = $this->request->input('first_name');
        $user->last_name = $this->request->input('last_name');
        $user->email = $this->request->input('email');
        $user->password = Hash::make($this->request->input('password'));
        $user->image_path = 'img/no_image_avatar.png';
        $user->save();

        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Instantiate taken business model for creating a new auth token.
         *
         -------------------------------------------------------------------------------------------------------------*/
        $this->tokenBusinessModel = new TokenBusinessModel($user);
        if ($this->tokenBusinessModel->resolve())
        {
            return $this->responseSuccess($user);
        }

        return $this->responseFail();
    }

    /**
     * @param User $user
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseSuccess (User $user)
    {
        /*---------------------------------------------------------------------------------------------------------10101
         *
         * Prepare user to be returned to frontend as array
         *
         -------------------------------------------------------------------------------------------------------------*/
        $user->token = $this->tokenBusinessModel->getPlainToken();
        $frontUser = $user->toArray();
        unset($frontUser['access_token']);

        return response()->json([
            'code'    => Response::GENERAL_SUCCESS,
            'data'    => $frontUser,
            'message' => []
        ]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function responseFail ()
    {
        return response()->json([
            'code' => Response::GENERAL_FAIL,
            'message' => 'Some error occurred trying to save the new user. Please try again.'
        ]);
    }
}