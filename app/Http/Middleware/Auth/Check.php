<?php

namespace App\Http\Middleware\Auth;

use App\Models\User;
use Closure;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * Class Check
 *
 * @package App\Http\Middleware\Auth
 */
class Check
{
    /**
     * @var User
     */
    protected static $user;

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        try
        {
            self::$user = User::findByUid($request->header('Customer-Uid'));

            /*-----------------------------------------------------------------------------------------------------10101
             *
             * Check if customer token it is belonging to user
             *
             ---------------------------------------------------------------------------------------------------------*/
            if(Hash::check($request->header('Customer-Token'), self::$user->accessToken->token))
            {
                return $next($request);
            }

            throw new AuthenticationException('Unauthenticated.');
        }
        catch(ModelNotFoundException $e)
        {
            throw new AuthenticationException('Unauthenticated.');
        }
    }
}