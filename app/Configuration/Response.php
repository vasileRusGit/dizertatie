<?php

namespace App\Configuration;

/**
 * Class Response
 *
 * @package App\Configuration
 */
class Response
{
    const GENERAL_SUCCESS = 100;

    const GENERAL_FAIL = 500;
}