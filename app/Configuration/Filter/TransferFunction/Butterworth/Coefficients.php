<?php

namespace App\Configuration\Filter\TransferFunction\Butterworth;

/**
 * Class Coefficients
 *
 * @package App\Configuration\Filter\TransferFunction\Butterworth
 */
class Coefficients
{
    const ORDER_COEFFICIENTS = [
        '1' => [
            1,
            1
        ],

        '2' => [
            1,
            1.414214,
            1
        ],

        '3' => [
            1,
            2,
            2,
            1
        ],

        '4' => [
            1,
            2.613126,
            3.414214,
            2.613126,
            1
        ],

        '5' => [
            1,
            3.236068,
            5.236068,
            5.236068,
            3.236068,
            1
        ],

        '6' => [
            1,
            3.863703,
            7.464102,
            9.14162,
            7.464102,
            3.863703,
            1
        ],

        '7' => [
            1,
            4.493959,
            10.09784,
            14.59179,
            14.59179,
            10.09784,
            4.493959,
            1
        ],

        '8' => [
            1,
            5.125831,
            13.13707,
            21.84615,
            25.68836,
            21.84615,
            13.13707,
            5.125831,
            1
        ],

        '9' => [
            1,
            5.75877,
            16.58172,
            31.16344,
            41.98639,
            41.98639,
            31.16344,
            16.58172,
            5.75877,
            1
        ],

        '10' => [
            1,
            6.392453,
            20.43173,
            42.80206,
            64.8824,
            74.23343,
            64.8824,
            42.80206,
            20.43173,
            6.392453,
            1
        ]
    ];

    /**
     * Return order coefficients
     *
     * @param $order
     *
     * @return array
     */
    public static function getTransferFunctionCoefficients($order) : array
    {
        foreach(self::ORDER_COEFFICIENTS as $key => $coefficient)
        {
            if($order === $key)
            {
                return $coefficient;
            }
        }
    }
}