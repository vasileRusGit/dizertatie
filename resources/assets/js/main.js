import "./bootstrap";
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App'

// vue event bus
import VueBus from 'vue-bus';
Vue.use(VueBus);

// vue graphs
import VueGraph from 'vue-graph'
Vue.use(VueGraph);

// router setup
import routes from './routes/routes'

// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import Notifications from './components/NotificationPlugin'

// MaterialDashboard plugin
import MaterialDashboard from './material-dashboard'

import Chartist from 'chartist'

Vue.use(VueRouter);
Vue.use(MaterialDashboard);
Vue.use(GlobalComponents);
Vue.use(GlobalDirectives);
Vue.use(Notifications);

// configure router
const router = new VueRouter({
    mode: 'history',
    routes, // short for routes: routes
    linkExactActiveClass: 'nav-item active'
});

// run check-auth before each request
router.beforeEach((to, from, next) => {
    if (to.meta.requiresAuth) {
        // check if uid and token are null
        if(localStorage.getItem('uid') === 'null' && localStorage.getItem('token') === 'null' ||
            localStorage.getItem('uid') === null && localStorage.getItem('token') === null) {
            window.axios.defaults.headers.common['Customer-Uid'] = localStorage.getItem('uid');
            window.axios.defaults.headers.common['Customer-Token'] = localStorage.getItem('token');
            axios.get('/check-auth')
                .then(response => {
                    if (response.data.code === 100) {
                        next()
                    }
                    else if (response.data.code === 500) {
                        router.push({name: 'Login'})
                    }
                    next()
                }).catch(error => {
                console.log(error);
            });
            next()
        }
        else {
            next()
        }
    }
    else {
        next()
    }
});

// condition that before each request c=make sure that customer uid and token are set
router.beforeEach((to, from, next) => {
    if(window.axios.defaults.headers.common['Customer-Uid'] === 'null' || window.axios.defaults.headers.common['Customer-Token'] === 'null') {
        window.axios.defaults.headers.common['Customer-Uid'] = localStorage.getItem('uid');
        window.axios.defaults.headers.common['Customer-Token'] = localStorage.getItem('token');
    }

    next();
});


// global library setup
Object.defineProperty(Vue.prototype, '$Chartist', {
    get() {
        return this.$root.Chartist
    }
});

/* eslint-disable no-new */
new Vue({
    el: '#app',
    render: h => h(App),
    router,
    data: {
        Chartist: Chartist
    }
});
