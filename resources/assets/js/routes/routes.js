import DashboardLayout from '@/pages/Layout/DashboardLayout.vue'
import AuthLayout from '@/pages/Layout/AuthLayout.vue'

import Dashboard from '@/pages/Dashboard.vue'
import UserProfile from '@/pages/UserProfile.vue'
import FilterDesign from '@/pages/Filter/Design.vue'
import FilterResults from '@/pages/Filter/Results.vue'
import ButterworthTransferFunctionValues from '@/pages/TransferFunctionValues/Butterworth.vue'
import TableList from '@/pages/TableList.vue'
import Typography from '@/pages/Typography.vue'
import Login from '@/pages/Auth/Login.vue'
import Register from '@/pages/Auth/Register.vue'
import Icons from '@/pages/Icons.vue'
import Maps from '@/pages/Maps.vue'
import Notifications from '@/pages/Notifications.vue'

const routes = [
    {
        path: '/',
        component: DashboardLayout,
        redirect: '/dashboard',
        children: [
            {
                path: 'dashboard',
                name: 'Dashboard',
                component: Dashboard,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'user',
                name: 'User Profile',
                component: UserProfile,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'filter/design',
                name: 'Design Filter',
                component: FilterDesign,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'filter/results',
                name: 'Filter Results',
                component: FilterResults,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'butterworth/transfer/function/values',
                name: 'Butterworth Transfer Function Values',
                component: ButterworthTransferFunctionValues,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'table',
                name: 'Table List',
                component: TableList,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'typography',
                name: 'Typography',
                component: Typography,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'icons',
                name: 'Icons',
                component: Icons,
                meta: {
                    requiresAuth: true
                }
            },
            {
                path: 'maps',
                name: 'Maps',
                meta: {
                    hideFooter: true,
                    requiresAuth: true
                },
                component: Maps
            },
            // {
            //     path: 'notifications',
            //     name: 'Notifications',
            //     component: Notifications,
            //     meta: {
            //         requiresAuth: true
            //     }
            // }
        ]
    },
    {
        path: '/',
        component: AuthLayout,
        redirect: '/login',
        children: [
            {
                path: 'login',
                name: 'Login',
                component: Login
            }
        ]
    },
    {
        path: '/',
        component: AuthLayout,
        redirect: '/register',
        children: [
            {
                path: 'register',
                name: 'Register',
                component: Register
            }
        ]
    },
];

export default routes
