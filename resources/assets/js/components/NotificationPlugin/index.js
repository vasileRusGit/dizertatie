import Notifications from './Notifications.vue'

const NotificationStore = {
    state: {}, // here the notifications will be added

    removeNotification() {
        this.state = {}
    },

    showNotification(message, type, uid) {
        this.state = {
            'message': message,
            'type': type,
            'uid': uid
        };

        // remove the unclosed notification
        setTimeout(function(){ this.removeNotification() }.bind(this), 7000);
    },

    showValidationNotification(error) {
        let errorMessages = '';
        let errors = Object.keys(error.response.data.errors).map(function (key) { return error.response.data.errors[key]; });
        errors.forEach(function (error) {
            errorMessages += error + '<br />';
        });

        this.state = {
            'message': errorMessages,
            'type': 'danger',
            'uid': '_' + Math.random().toString(36)
        };

        // remove the unclosed notification
        setTimeout(function(){ this.removeNotification() }.bind(this), 7000);
    },

    notify(notification) {
        if (Array.isArray(notification)) {
            notification.forEach((notificationInstance) => {
                this.showNotification(notificationInstance)
            })
        } else {
            this.showNotification(notification)
        }
    }
};

var NotificationsPlugin = {

    install(Vue) {
        Vue.mixin({
            data() {
                return {
                    notificationStore: NotificationStore
                }
            },
            methods: {
                notify(notification) {
                    this.notificationStore.notify(notification)
                }
            }
        });
        Object.defineProperty(Vue.prototype, '$notify', {
            get() {
                return this.$root.notify
            }
        });
        Object.defineProperty(Vue.prototype, '$notifications', {
            get() {
                return this.$root.notificationStore
            }
        });
        Vue.component('Notifications', Notifications)
    }
};

export default NotificationsPlugin
