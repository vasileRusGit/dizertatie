const LoadingStore = {

    isLoading: false,

    setLoading(bool) {
        this.isLoading = bool
    }
};

const LoadingPlugin = {

    install(Vue) {
        Vue.mixin({
            data() {
                return {
                    loadingStore: LoadingStore
                }
            }
        });

        Object.defineProperty(Vue.prototype, '$loadingStore', {
            get() {
                return this.$root.loadingStore
            }
        });
    }
};

export default LoadingPlugin
