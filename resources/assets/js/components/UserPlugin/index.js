const UserStore = {

    user: [],

    setUser(user) {
        this.user = user
    },

    destroy() {
        this.user = []
    }
};

const UserPlugin = {

    install(Vue) {
        Vue.mixin({
            data() {
                return {
                    userStore: UserStore
                }
            }
        });

        Object.defineProperty(Vue.prototype, '$userStore', {
            get() {
                return this.$root.userStore
            }
        });
    }
};

export default UserPlugin
