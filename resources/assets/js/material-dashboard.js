// Sidebar on the right. Used as a local plugin in DashboardLayout.vue
import SideBar from './components/SidebarPlugin'
import User from './components/UserPlugin'
import Loading from './components/LoadingPlugin'

// asset imports
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import './assets/scss/material-dashboard.scss'

// library auto imports
import 'es6-promise/auto'

/**
 * This is the main Light Bootstrap Dashboard Vue plugin where dashboard related plugins are registered.
 */
export default{
  install (Vue) {
    Vue.use(SideBar);
    Vue.use(User);
    Vue.use(Loading);
    Vue.use(VueMaterial)
  }
}
